package com.example.socialmediaapp.user;

import com.example.socialmediaapp.EntityNotFoundException;
import com.example.socialmediaapp.post.Post;
import com.example.socialmediaapp.post.PostController;
import com.example.socialmediaapp.post.PostRepository;
import com.example.socialmediaapp.userrelationship.UserRelationship;
import com.example.socialmediaapp.userrelationship.UserRelationshipRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserRelationshipRepository userRelationshipRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreatePost() {
        User user = new User();
        user.setId(1L);
        Post post = new Post();
        post.setTitle("Test Title");
        post.setBody("Test Body");
        post.setAuthor(user);

        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(postRepository.save(any(Post.class))).thenReturn(post);

        ResponseEntity<String> response = userController.createPost(1L, post);

        verify(userRepository, times(1)).findById(1L);
        verify(postRepository, times(1)).save(any(Post.class));
        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    void testCreatePostUserNotFound() {
        User user = new User();
        user.setId(1L);

        Post post = new Post();

        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            userController.createPost(user.getId(), post);
        });

        verify(userRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void testGetUserPosts() {
        User user = new User();
        user.setId(1L);
        List<Post> userPosts = new ArrayList<>();
        userPosts.add(new Post("Post 1", "Body 1", user));
        userPosts.add(new Post("Post 2", "Body 2", user));

        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(postRepository.findByAuthor(user)).thenReturn(userPosts);

        List<Post> response = userController.getUserPosts(1L);

        verify(userRepository, times(1)).findById(1L);
        verify(postRepository, times(1)).findByAuthor(user);
        assertEquals(response.size(), 2);
    }

    @Test
    public void testGetUserPostsUserNotFound() {
        User user = new User();
        user.setId(1L);

        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            userController.getUserPosts(1L);
        });

        verify(userRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(userRepository);
        verifyNoInteractions(postRepository);
    }

    @Test
    public void testFollowUser() {
        User follower = new User();
        follower.setId(1L);
        User followed = new User();
        followed.setId(2L);

        when(userRepository.findById(1L)).thenReturn(Optional.of(follower));
        when(userRepository.findById(2L)).thenReturn(Optional.of(followed));
        when(userRelationshipRepository.findByFollowerAndFollowed(follower, followed)).thenReturn(null);
        when(userRelationshipRepository.save(any(UserRelationship.class))).thenReturn(new UserRelationship(follower, followed));

        ResponseEntity<String> response = userController.followUser(1L, 2L);

        verify(userRepository, times(2)).findById(any(Long.class));
        verify(userRelationshipRepository, times(1)).findByFollowerAndFollowed(follower, followed);
        verify(userRelationshipRepository, times(1)).save(any(UserRelationship.class));
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testFollowUserAlreadyFollowing() {
        User follower = new User();
        follower.setId(1L);
        User followed = new User();
        followed.setId(2L);

        when(userRepository.findById(1L)).thenReturn(Optional.of(follower));
        when(userRepository.findById(2L)).thenReturn(Optional.of(followed));
        when(userRelationshipRepository.findByFollowerAndFollowed(follower, followed)).thenReturn(new UserRelationship(follower, followed));

        ResponseEntity<String> response = userController.followUser(1L, 2L);

        verify(userRepository, times(2)).findById(any(Long.class));
        verify(userRelationshipRepository, times(1)).findByFollowerAndFollowed(follower, followed);

        // Verify that the controller returns the expected response
        assertEquals(response.getStatusCodeValue(), 200);
        assertEquals(response.getBody(), "You are already following " + followed.getUsername());
    }

    @Test
    public void testFollowUserNotFollowing() {
        User follower = new User();
        follower.setId(1L);
        User followed = new User();
        followed.setId(2L);

        when(userRepository.findById(1L)).thenReturn(Optional.of(follower));
        when(userRepository.findById(2L)).thenReturn(Optional.of(followed));
        when(userRelationshipRepository.findByFollowerAndFollowed(follower, followed)).thenReturn(null);
        when(userRelationshipRepository.save(any(UserRelationship.class))).thenReturn(new UserRelationship(follower, followed));

        ResponseEntity<String> response = userController.followUser(1L, 2L);

        verify(userRepository, times(2)).findById(any(Long.class));
        verify(userRelationshipRepository, times(1)).findByFollowerAndFollowed(follower, followed);
        verify(userRelationshipRepository, times(1)).save(any(UserRelationship.class));

        // Verify that the controller returns the expected response
        assertEquals(response.getStatusCodeValue(), 200);
        assertEquals(response.getBody(), "You are now following " + followed.getUsername());
    }


    @Test
    public void testFollowUserFollowerNotFound() {
        User followed = new User();
        followed.setId(2L);

        when(userRepository.findById(1L)).thenReturn(Optional.empty());
        when(userRepository.findById(2L)).thenReturn(Optional.of(followed));

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            userController.followUser(1L, 2L);
        });


        verify(userRepository, times(1)).findById(any(Long.class));
        verifyNoMoreInteractions(userRepository);
        verifyNoInteractions(userRelationshipRepository);

    }

    @Test
    public void testFollowUserFollowedNotFound() {
        User follower = new User();
        follower.setId(1L);

        when(userRepository.findById(1L)).thenReturn(Optional.of(follower));
        when(userRepository.findById(2L)).thenReturn(Optional.empty());

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            userController.followUser(1L, 2L);
        });


        verify(userRepository, times(2)).findById(any(Long.class));
        verifyNoMoreInteractions(userRepository);
        verifyNoInteractions(userRelationshipRepository);

    }
}