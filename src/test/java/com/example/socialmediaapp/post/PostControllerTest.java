package com.example.socialmediaapp.post;

import com.example.socialmediaapp.EntityNotFoundException;
import com.example.socialmediaapp.user.User;
import com.example.socialmediaapp.user.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class PostControllerTest {

    @InjectMocks
    private PostController postController;

    @Mock
    private PostRepository postRepository;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLikePostUserAlreadyLiked() {
        User user = new User();
        user.setId(1L);

        // Create a post with the user already liked
        Post post = new Post();
        post.setId(1L);
        post.setTitle("Test Title");
        post.setBody("Test Body");
        post.setAuthor(user);

        Set<User> likedBy = new HashSet<>();
        likedBy.add(user);
        post.setLikedBy(likedBy);

        when(postRepository.findById(1L)).thenReturn(Optional.of(post));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        ResponseEntity<String> response = postController.likePost(1L, 1L);

        verify(postRepository, times(1)).findById(1L);
        verify(userRepository, times(1)).findById(1L);

        // Verify that the controller returns the expected response
        assertEquals(response.getStatusCodeValue(), 200);
        assertEquals(response.getBody(), "You have already liked this post.");
    }

    @Test
    public void testLikePostUserNotLiked() {
        User user = new User();
        user.setId(1L);

        // Create a post with no users who liked it
        Post post = new Post();
        post.setId(1L);
        post.setTitle("Test Title");
        post.setBody("Test Body");
        post.setAuthor(user);
        post.setLikedBy(new HashSet<>());

        when(postRepository.findById(1L)).thenReturn(Optional.of(post));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(postRepository.save(any(Post.class))).thenReturn(post);

        ResponseEntity<String> response = postController.likePost(1L, 1L);

        verify(postRepository, times(1)).findById(1L);
        verify(userRepository, times(1)).findById(1L);
        verify(postRepository, times(1)).save(post);

        // Verify that the controller returns the expected response
        assertEquals(response.getStatusCodeValue(), 200);
        assertEquals(response.getBody(), "You liked the post with ID: 1");
    }

    @Test
    public void testUnlikePostUserAlreadyUnliked() {
        User user = new User();
        user.setId(1L);

        // Create a post with the user already unliked
        Post post = new Post();
        post.setId(1L);
        post.setTitle("Test Title");
        post.setBody("Test Body");
        post.setAuthor(user);

        // No users liked the post
        post.setLikedBy(new HashSet<>());

        when(postRepository.findById(1L)).thenReturn(Optional.of(post));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        ResponseEntity<String> response = postController.unlikePost(1L, 1L);

        verify(postRepository, times(1)).findById(1L);
        verify(userRepository, times(1)).findById(1L);

        // Verify that the controller returns the expected response
        assertEquals(response.getStatusCodeValue(), 200);
        assertEquals(response.getBody(), "You have not liked this post.");
    }

    @Test
    public void testUnlikePostUserLiked() {
        User user = new User();
        user.setId(1L);

        // Create a post with the user liked
        Post post = new Post();
        post.setId(1L);
        post.setTitle("Test Title");
        post.setBody("Test Body");
        post.setAuthor(user);

        Set<User> likedBy = new HashSet<>();
        likedBy.add(user);
        post.setLikedBy(likedBy);

        when(postRepository.findById(1L)).thenReturn(Optional.of(post));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(postRepository.save(any(Post.class))).thenReturn(post);

        ResponseEntity<String> response = postController.unlikePost(1L, 1L);

        verify(postRepository, times(1)).findById(1L);
        verify(userRepository, times(1)).findById(1L);
        verify(postRepository, times(1)).save(post);

        // Verify that the controller returns the expected response
        assertEquals(response.getStatusCodeValue(), 200);
        assertEquals(response.getBody(), "You unliked the post with ID: 1");
    }

    @Test
    public void testLikePostPostNotFound() {
        User user = new User();
        user.setId(1L);
        long postId = 1L;

        when(postRepository.findById(postId)).thenReturn(Optional.empty());

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            postController.likePost(postId, user.getId());
        });

        verify(postRepository, times(1)).findById(postId);
        verifyNoMoreInteractions(userRepository, postRepository);
    }

    @Test
    public void testLikePostUserNotFound() {
        User user = new User();
        user.setId(1L);
        long postId = 1L;
        long userId = 2L;

        when(postRepository.findById(postId)).thenReturn(Optional.of(new Post()));
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            postController.likePost(postId, userId);
        });

        verify(postRepository, times(1)).findById(postId);
        verify(userRepository, times(1)).findById(userId);
        verifyNoMoreInteractions(postRepository);
    }

    @Test
    public void testUnlikePostPostNotFound() {
        User user = new User();
        user.setId(1L);
        long postId = 1L;

        when(postRepository.findById(postId)).thenReturn(Optional.empty());

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            postController.unlikePost(postId, user.getId());
        });

        verify(postRepository, times(1)).findById(postId);
        verifyNoMoreInteractions(userRepository, postRepository);
    }


    @Test
    public void testUnlikePostUserNotFound() {
        User user = new User();
        user.setId(1L);
        long postId = 1L;
        long userId = 2L;

        when(postRepository.findById(postId)).thenReturn(Optional.of(new Post()));
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Use assertThrows to verify that EntityNotFoundException is thrown
        assertThrows(EntityNotFoundException.class, () -> {
            postController.unlikePost(postId, userId);
        });

        verify(postRepository, times(1)).findById(postId);
        verify(userRepository, times(1)).findById(userId);
        verifyNoMoreInteractions(postRepository);
    }
}