package com.example.socialmediaapp.user;

import com.example.socialmediaapp.EntityNotFoundException;
import com.example.socialmediaapp.post.Post;
import com.example.socialmediaapp.post.PostRepository;
import com.example.socialmediaapp.userrelationship.UserRelationship;
import com.example.socialmediaapp.userrelationship.UserRelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRelationshipRepository userRelationshipRepository;

    // Create a new post for a user
    @PostMapping("/{userId}/posts")
    public ResponseEntity<String> createPost(@PathVariable Long userId, @RequestBody Post post) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(User.class, userId));

        post.setAuthor(user);
        postRepository.save(post);

        return ResponseEntity.status(HttpStatus.CREATED).body("Post created successfully");
    }

    // Get a user's posts
    @GetMapping("/{userId}/posts")
    public List<Post> getUserPosts(@PathVariable Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(User.class, userId));

        return postRepository.findByAuthor(user);
    }

    // Follow another user
    @PostMapping("/{followerId}/follow/{followedId}")
    public ResponseEntity<String> followUser(@PathVariable Long followerId, @PathVariable Long followedId) {
        User follower = userRepository.findById(followerId)
                .orElseThrow(() -> new EntityNotFoundException(User.class, followerId));

        User followed = userRepository.findById(followedId)
                .orElseThrow(() -> new EntityNotFoundException(User.class, followedId));

        // Check if the relationship already exists to prevent duplicates
        if (userRelationshipRepository.findByFollowerAndFollowed(follower, followed) != null) {
            return ResponseEntity.ok("You are already following " + followed.getUsername());
        }

        UserRelationship userRelationship = new UserRelationship(follower, followed);
        userRelationshipRepository.save(userRelationship);

        return ResponseEntity.ok("You are now following " + followed.getUsername());
    }
}
