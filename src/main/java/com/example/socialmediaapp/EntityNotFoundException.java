package com.example.socialmediaapp;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(Class<?> entityClass, Long entityId) {
        super("Entity of type " + entityClass.getSimpleName() + " with ID " + entityId + " not found.");
    }
}
