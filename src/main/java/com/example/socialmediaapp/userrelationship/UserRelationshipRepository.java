package com.example.socialmediaapp.userrelationship;

import com.example.socialmediaapp.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRelationshipRepository extends JpaRepository<UserRelationship, Long> {
    UserRelationship findByFollowerAndFollowed(User follower, User followed);
    // Add more custom query methods if needed.
}
