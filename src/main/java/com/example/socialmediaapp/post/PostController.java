package com.example.socialmediaapp.post;

import com.example.socialmediaapp.EntityNotFoundException;
import com.example.socialmediaapp.user.User;
import com.example.socialmediaapp.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/posts")
public class PostController {
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    // Like a post
    @PostMapping("/{postId}/like/{userId}")
    public ResponseEntity<String> likePost(@PathVariable Long postId, @PathVariable Long userId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new EntityNotFoundException(Post.class, postId));

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(User.class, userId));

        if (post.getLikedBy().contains(user)) {
            return ResponseEntity.ok("You have already liked this post.");
        }

        post.getLikedBy().add(user);
        postRepository.save(post);

        return ResponseEntity.ok("You liked the post with ID: " + postId);
    }

    // Unlike a post
    @PostMapping("/{postId}/unlike/{userId}")
    public ResponseEntity<String> unlikePost(@PathVariable Long postId, @PathVariable Long userId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new EntityNotFoundException(Post.class, postId));

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(User.class, userId));

        if (!post.getLikedBy().contains(user)) {
            return ResponseEntity.ok("You have not liked this post.");
        }

        post.getLikedBy().remove(user);
        postRepository.save(post);

        return ResponseEntity.ok("You unliked the post with ID: " + postId);
    }
}



