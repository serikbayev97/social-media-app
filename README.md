# Social Media Application

## Description

This is a simple social media application built using Spring Boot, Hibernate, and PostgreSQL. The application allows users to create and view posts, follow other users, and like posts. Each post has a title, body, and author.

## Prerequisites

- Java 11
- PostgreSQL 15.1
- Maven 3.6.3

## How to Run

1. **Database Configuration**:
   - Create a PostgreSQL database named `social_media_db`.
   - Update the database credentials in `src/main/resources/application.properties` file if needed.

2. **Build and Run**:
   - Navigate to the project directory using the terminal.
   - Use the following Maven command to build and run the application:

   ```bash
   mvn spring-boot:run

# Feedback

- Was it easy to complete the task using AI? *Yes, it was straightforward.*
- How long did the task take you to complete? *Around 1 hour.*
- Was the code ready to run after generation? What did you have to change to make it usable? *The generated content provided a good starting point but required customization.*
- Which challenges did you face during completion of the task? *Covering 80% of code with unit tests.* 
- Which specific prompts you learned as a good practice to complete the task? *Put commented line by ChatGPT to next question for comprehensive implementation.* 